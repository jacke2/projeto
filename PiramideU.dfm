object PiramideForm: TPiramideForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Teste Devio'
  ClientHeight = 593
  ClientWidth = 603
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PiramidePanel: TPanel
    Left = 0
    Top = 0
    Width = 603
    Height = 125
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 222
      Top = 11
      Width = 149
      Height = 23
      Alignment = taCenter
      Caption = 'Asterisk Pyramid'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 255
      Top = 45
      Width = 78
      Height = 13
      Caption = 'Asterisk Amount'
    end
    object GenerateButton: TButton
      Left = 256
      Top = 91
      Width = 75
      Height = 25
      Caption = 'GENERATE'
      TabOrder = 0
      OnClick = GenerateButtonClick
    end
    object AsteriskEdit: TEdit
      Left = 256
      Top = 63
      Width = 75
      Height = 21
      TabOrder = 1
      OnKeyPress = AsteriskEditKeyPress
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 125
    Width = 603
    Height = 468
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    Color = clMenuBar
    Enabled = False
    TabOrder = 1
  end
end
