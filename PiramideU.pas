unit PiramideU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TPiramideForm = class(TForm)
    PiramidePanel: TPanel;
    Label1: TLabel;
    GenerateButton: TButton;
    AsteriskEdit: TEdit;
    Memo1: TMemo;
    Label2: TLabel;
    procedure GenerateButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsteriskEditKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PiramideForm: TPiramideForm;

implementation

{$R *.dfm}

procedure TPiramideForm.AsteriskEditKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9', #8, #13]) then
    key :=#0
  else if key = #13 then
    GenerateButton.OnClick(nil);
end;

procedure TPiramideForm.GenerateButtonClick(Sender: TObject);
var
  count : Integer;
  asteriskCount : Integer;
  loEspacos : Integer;
begin
  if AsteriskEdit.Text = '' then
  begin
    showmessage('Digite um n�mero!');
    AsteriskEdit.SetFocus;
    exit;
  end;

  Memo1.lines.Clear;
  asteriskCount := StrToInt(AsteriskEdit.Text);

  //asteriskCount > 35 - devido ao seu tamanho o formul�rio ir� mostrar somente as 35 primeiras linhas
  for count := 1 to asteriskCount do
  begin
    loEspacos :=  97 - count;
    Memo1.lines.add(StringOfChar(' ',loEspacos) + StringOfChar('*',count) + StringOfChar(' ',loEspacos));
  end;

  Memo1.SelStart := 0;
  Memo1.SelLength := 0;

end;

procedure TPiramideForm.FormShow(Sender: TObject);
begin
  AsteriskEdit.SetFocus;
end;

end.

